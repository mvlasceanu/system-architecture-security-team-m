<?php
/**
 * Mihai-Marius Vlasceanu
 * <p>
 * NOTICE OF LICENSE
 * <p>
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author Mihai-Marius Vlasceanu
 * @class CoreHelper
 * @file CoreHelper.php
 * @timestamp 16-Mar-15 03:44
 * @copyright Copyright (c) 2015 Mihai-Marius Vlasceanu (mihaivlasceanu@gmail.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class CoreHelper {
    /**
     * @param int $length
     * @return string
     */
    public static function generateString($length = 15)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-';
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    /**
     * @param $distant_timestamp
     * @param int $max_units
     * @return string
     */
    public static function getTimeSince($distant_timestamp, $max_units = 3) {
        $i = 0;
        $time = time() - strtotime($distant_timestamp   ); // to get the time since that moment
        $tokens = array(
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        if($time == 0) {
            return Yii::t("app", "Just now");
        }

        $responses = array();
        while ($i < $max_units) {
            foreach ($tokens as $unit => $text) {
                if ($time + 1 < $unit) {
                    continue;
                }
                $i++;
                $numberOfUnits = floor($time / $unit);

                $responses[] = $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
                $time -= ($unit * $numberOfUnits);
                break;
            }
        }

        if (!empty($responses)) {
            return implode(', ', $responses) . ' ago';
        }

        return 'Just now';
    }
}