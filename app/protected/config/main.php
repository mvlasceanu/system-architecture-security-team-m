<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
Yii::setPathOfAlias("helpers", dirname(__FILE__) . '/../helpers');
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'SSAS - Photo Sharing',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'application.modules.*',
        'application.helpers.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.image.models.*',
        'application.modules.image.helpers.*',
        'application.extensions.EPhpThumb.EPhpThumb',
        'helpers.*',
    ),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
        'user',
        'image',
        'comments',
        'imageShare',
	),

	// application components
	'components'=>array(
        'user'=>array(
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
            'loginUrl' => array('/user/login'),
        ),
        'clientScript'                  =>array(
            'coreScriptPosition'        => CClientScript::POS_END,
            'class'                     => 'CClientScript',
            'defaultScriptFilePosition' => CClientScript::POS_END,
            'defaultScriptPosition'     => CClientScript::POS_READY,
            'enableJavaScript'          => true,
        ),

		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
                'image/<controller:\w+>/<action:\w+>'           => 'image/<controller>/<action>',
                'image/<controller:\w+>/<action:\w+>/<v:\w+>'   => 'image/view/index',
                'user/view/<id:\w+>'                            => 'user/user/view',
				'<controller:\w+>/<id:\d+>'                     => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'        => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>'                 => '<controller>/<action>',
			),
			'showScriptName'=>false,
		),
		

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
        'phpThumb'=>array(
            'class'=>'application.extensions.EPhpThumb.EPhpThumb',
            'options'=>array()
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'mihaivlasceanu@gmail.com',
	),
);
