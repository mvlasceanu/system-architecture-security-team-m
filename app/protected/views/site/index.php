<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<?php if(!Yii::app()->user->isGuest): ?>
    <fieldset>
        <legend><?php echo Yii::t("app", "Pictures shared with you"); ?></legend>

    <?php if(sizeof($shares) == 0): ?>
    <p>
        <?php echo Yii::t("app", "Sorry, there is nothing to show :("); ?>
    </p>
    <?php endif; ?>
    <?php foreach($shares AS $share): ?>
        <div class="picture-thumbnail">
            <a href="<?php echo $share->image->getControllerUrl(); ?>">
                <img src="<?php echo $share->image->getThumbnailUrl(200, 200); ?>" />
            </a>
        </div>
    <?php endforeach; ?>
    </fieldset>
<?php endif; ?>

<fieldset>
    <legend><?php echo Yii::t("app", "Public pictures"); ?></legend>

    <?php if(sizeof($public_pictures) == 0): ?>
        <p>
            <?php echo Yii::t("app", "Sorry, there is nothing to show :("); ?>
        </p>
    <?php endif; ?>
    <?php foreach($public_pictures AS $picture): ?>
        <div class="picture-thumbnail">
            <a href="<?php echo $picture->getControllerUrl(); ?>">
                <img src="<?php echo $picture->getThumbnailUrl(200, 200); ?>" />
            </a>
        </div>
    <?php endforeach; ?>
</fieldset>