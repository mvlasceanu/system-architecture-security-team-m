<?php

class ActivationController extends Controller
{
	public $defaultAction = 'activation';

	
	/**
	 * Activation user account
	 */
	public function actionActivation () {
        // Retrieve request object
        $request = Yii::app()->request;

        // Get the e-mail
		$email = $request->getParam("email");

        // Get activation key
		$activation_key = $request->getParam("activation_key");

		if ($email && $activation_key) {
            // Try to load the model based on the e-mail
			$find = User::model()->notsafe()->findByAttributes(array('email'=>$email));

			if (isset($find) && $find->status) {
			    $this->render('/user/message', array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("You account is active.")));
			} elseif(isset($find->activation_key) && ($find->activation_key == $activation_key)) {
				$find->activation_key = UserModule::encrypting(microtime());
				$find->status = UserModule::STATUS_ACTIVE;
				$find->save();
			    $this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("You account is activated.")));
			} else {
			    $this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("Incorrect activation URL.")));
			}
		} else {
			$this->render('/user/message',array('title'=>UserModule::t("User activation"),'content'=>UserModule::t("Incorrect activation URL.")));
		}
	}

}