<?php
/**
 * Mihai-Marius Vlasceanu
 * <p>
 * NOTICE OF LICENSE
 * <p>
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author Mihai-Marius Vlasceanu
 * @class ApiController
 * @file ApiController.php
 * @timestamp 21-Mar-15 12:50
 * @copyright Copyright (c) 2015 Mihai-Marius Vlasceanu (mihaivlasceanu@gmail.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class ApiController extends Controller {

    /**\
     *
     */
    public function actionGetUsersWithoutSelf()
    {
        // Request object
        $request = Yii::app()->request;

        // Current user
        $currentUser = Yii::app()->user;

        // Throw error if user is not logged in
        if($currentUser->isGuest)
        {
            $response = array();
            $response["error"] = new CHttpException(403, Yii::t("app", "Invalid request: You must be logged in to use this API"));
            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Get all users in db
        $users = User::getAllUsers();

        // Init response array
        $response = array();
        $response["users"] = array();

        // Add users to array
        foreach($users AS $user)
        {
            // Except current user
            if($user->id == $currentUser->id)
                continue;

            $response["users"][] = $user;
        }

        // Display data in json format
        echo CJSON::encode($response);

        // Required by Yii
        Yii::app()->end();

    }

    /**
     * Retrieves all users and marks down all of those that already have
     * access to image
     */
    public function actionGetUsersWithoutSelfWithShare()
    {
        // Request object
        $request = Yii::app()->request;

        // Response array init
        $response = array();

        // Current user
        $currentUser = Yii::app()->user;

        // Throw error if user is not logged in
        if($currentUser->isGuest)
        {
            $response["error"] = new CHttpException(403, Yii::t("app", "Invalid request: You must be logged in to use this API"));
            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Image id query param
        $image_string_id = (preg_match(ImageHelper::IMAGE_STR_ID_REGEX, $request->getQuery("image")) ? $request->getQuery("image") : false);

        // Throw exception is string is invalid
        if(!$image_string_id) {
            $response["error"] = new CHttpException(403, Yii::t("app", "Invalid image id."));
            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Load image model of the share
        $image = Image::getImageByStringIdentifier($image_string_id);

        // Throw exception if image does not exist
        if(!$image)
        {
            $response["error"] = new CHttpException(404, Yii::t("app", "Image with id $image_string_id does not exist."));
            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Throw exception is user is not the owner
        if(!$image->isOwner($currentUser->id) && !Yii::app()->getModule('user')->isAdmin())
        {
            $response["error"] = new CHttpException(403, Yii::t("app", "You are not authorized to do this."));
            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Get all users in db
        $users = User::getAllUsers();

        // Array to hold the ids of the users that already share
        $shares_user_array = array();

        // Get the sharers of the image
        $shares = $image->shares;

        // Add their IDs to the temp array
        foreach($shares AS $sh)
        {
            array_push($shares_user_array, $sh->user_id);
        }

        // Array key containing the users list
        $response["users"] = array();

        // Add users to array
        foreach($users AS $user)
        {
            // Except current user
            if($user->id == $currentUser->id)
                continue;

            // Convert the user objects to array
            $jsonUser = array();

            // Add user id
            $jsonUser["id"] = $user->id;

            // Add user name
            $jsonUser["username"] = $user->username;

            // If the user already shares the image, mark it
            if(in_array($user->id, $shares_user_array))
            {
                $jsonUser["ticked"] = true;
            }

            // Add to response array
            $response["users"][] = $jsonUser;
        }

        // JSON output
        echo (CJSON::encode($response));
        Yii::app()->end();

    }

    /**
     * Add a user share
     */
    public function actionAddShare()
    {
        // Request object
        $request = Yii::app()->request;

        // Response array
        $response = array();

        // Current logged in user
        $user = Yii::app()->user;

        // Throw error if user is not logged in
        if($user->isGuest)
        {
            $exception                  =  new CHttpException(403, Yii::t("app", "Invalid request: You must be logged in to use this API"));
            $response["error"]          = $exception->getCode();
            $response["errorMessage"]   = $exception->getMessage();
            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Image id query param
        $image_string_id = (preg_match(ImageHelper::IMAGE_STR_ID_REGEX, $request->getQuery("image")) ? $request->getQuery("image") : false);

        // Throw exception is string is invalid
        if(!$image_string_id) {
            $exception                  = new CHttpException(403, Yii::t("app", "Invalid image id."));
            $response["error"]          = $exception->getCode();
            $response["errorMessage"]   = $exception->getMessage();

            echo CJSON::encode($response);

            Yii::app()->end();
        }

        $image = Image::getImageByStringIdentifier($image_string_id);

        // Throw exception if image does not exist
        if(!$image)
        {
            $response["error"] = new CHttpException(404, Yii::t("app", "Image with id $image_string_id does not exist."));
            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Throw exception is user is not the owner
        if(!$image->isOwner($user->id) && !Yii::app()->getModule('user')->isAdmin())
        {
            $exception                  = new CHttpException(403, Yii::t("app", "You are not authorized to do this."));
            $response["error"]          = $exception->getCode();
            $response["errorMessage"]   = $exception->getMessage();

            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Check user id
        $user_share = ( (int) $request->getQuery("user_id") > 0) ? $request->getQuery("user_id") : false;

        // Throw exception if user id is incorrect
        if(!$user_share)
        {
            $exception                  = new CHttpException(403, Yii::t("app", "Incorrect share user ID " + $request->getQuery("user_id")));
            $response["error"]          = $exception->getCode();
            $response["errorMessage"]   = $exception->getMessage();

            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Add the user only if it has not been shared with already
        if(!ImageShare::isSharedWith($user_share, $image->id))
        {
            // ImageShare object
            $share = new ImageShare();

            // User id
            $share->user_id = $user_share;

            // Image id
            $share->image_id = $image->id;

            // Validate and save
            if($share->validate())
            {
                $share->save();
            } else {
                // Add errors to response
                $response["errors"] = $share->getErrors();
            }

            // Set privacy to custom
            $image->privacy = "custom";

            // Validate and save the image data
            if($image->validate()) {
                $image->save();

                // Ok message
                $response["message"] = "ok";
            } else {
                // Add errors to response
                $response["errors"] = $image->getErrors();
            }
        }

        echo CJSON::encode($response);

        Yii::app()->end();
    }

    /**
     * Remove users from share list
     */
    public function actionRemoveShare()
    {
        // Request object
        $request = Yii::app()->request;

        // Response array
        $response = array();

        // Current user
        $user = Yii::app()->user;

        // Throw error if user is not logged in
        if($user->isGuest)
        {
            $exception                  = new CHttpException(403, Yii::t("app", "Invalid request: You must be logged in to use this API"));
            $response["error"]          = $exception->getCode();
            $response["errorMessage"]   = $exception->getMessage();

            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Image id query param
        $image_string_id = (preg_match(ImageHelper::IMAGE_STR_ID_REGEX, $request->getQuery("image")) ? $request->getQuery("image") : false);

        // Throw exception is string is invalid
        if(!$image_string_id) {
            $exception                  = new CHttpException(403, Yii::t("app", "Invalid image id."));
            $response["error"]          = $exception->getCode();
            $response["errorMessage"]   = $exception->getMessage();

            echo CJSON::encode($response);

            Yii::app()->end();
        }

        $image = Image::getImageByStringIdentifier($image_string_id);

        // Throw exception if image does not exist
        if(!$image)
        {
            $exception                  = new CHttpException(404, Yii::t("app", "Image with id $image_string_id does not exist."));
            $response["error"]          = $exception->getCode();
            $response["errorMessage"]   = $exception->getMessage();

            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Throw exception is user is not the owner
        if(!$image->isOwner($user->id) && !Yii::app()->getModule('user')->isAdmin())
        {
            $exception                  = new CHttpException(403, Yii::t("app", "You are not authorized to do this."));
            $response["error"]          = $exception->getCode();
            $response["errorMessage"]   = $exception->getMessage();

            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Check user id
        $user_share = ((int) $request->getQuery("user_id") > 0) ? $request->getQuery("user_id") : false;

        // Throw exception if user id is incorrect
        if(!$user_share)
        {
            $exception                  = new CHttpException(403, Yii::t("app", "Incorrect share user ID " .$request->getQuery("user_id")));
            $response["error"]          = $exception->getCode();
            $response["errorMessage"]   = $exception->getMessage();

            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Delete user only if there is something to delete
        if(ImageShare::isSharedWith($user_share, $image->id))
        {
            // Load share object
            $share = ImageShare::getModelByUserAndImage($user_share, $image->id);

            // Additional check (Maybe not necessary?)
            if($share) {
                $share->delete();
            }

            // Update the image privacy to 'private' if there is nobody sharing
            if(sizeOf($image->shares) == 0) {

                // Set privacy
                $image->privacy = "private";

                // Validate and save
                if($image->validate()) {
                    $image->save();
                }
            }
            $response["message"] = "ok";

        }

        echo CJSON::encode($response);

        Yii::app()->end();
    }


    /**
     * Changes the privacy of an Image
     */
    public function actionChangePrivacy()
    {
        // Request object
        $request = Yii::app()->request;

        // Response array
        $response = array();

        // Current user
        $user = Yii::app()->user;

        // Throw error if user is not logged in
        if($user->isGuest)
        {
            $response["error"] = new CHttpException(403, Yii::t("app", "Invalid request: You must be logged in to use this API"));
            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Image id query param
        $image_string_id = (preg_match(ImageHelper::IMAGE_STR_ID_REGEX, $request->getQuery("image")) ? $request->getQuery("image") : false);

        // Throw exception is string is invalid
        if(!$image_string_id) {
            $response["error"] = new CHttpException(403, Yii::t("app", "Invalid image id."));
            echo CJSON::encode($response);

            Yii::app()->end();
        }

        $image = Image::getImageByStringIdentifier($image_string_id);

        // Throw exception if image does not exist
        if(!$image)
        {
            $response["error"] = new CHttpException(404, Yii::t("app", "Image with id $image_string_id does not exist."));
            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // New privacy setting
        $new_privacy = (preg_match("(" .ImageHelper::PRIVACY_PUBLIC. "|" .ImageHelper::PRIVACY_CUSTOM. "|" .ImageHelper::PRIVACY_PRIVATE. ")", $request->getQuery("privacy")) ? strtolower($request->getQuery("privacy")) : false);

        // Throw exception if privacy setting is not valid
        if(!$new_privacy)
        {
            $response["error"] = new CHttpException(403, Yii::t("app", "Invalid privacy type ".$request->getQuery("privacy"). ""));
            echo CJSON::encode($response);

            Yii::app()->end();
        }

        // Update only if they are not the same
        if($image->privacy !== trim($new_privacy)) {

            // Set new privacy
            $image->privacy = $new_privacy;

            // Validate and save
            if($image->validate()) {
                $image->save();

                // Some message
                $response["message"] = "ok";
            } else {
                $response["errors"] = $image->getErrors();
            }

            // Delete all the shares if the privacy is other than custom
            if($new_privacy == ImageHelper::PRIVACY_PUBLIC || $new_privacy == ImageHelper::PRIVACY_PRIVATE) {
                foreach ($image->shares AS $share) {
                    $share->delete();
                }
            }
        }

        echo CJSON::encode($response);

        Yii::app()->end();
    }
}