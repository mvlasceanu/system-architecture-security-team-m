<?php

/**
 * Class RegistrationController
 */
class RegistrationController extends Controller
{
	public $defaultAction = 'registration';
	


	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return (isset($_POST['ajax']) && $_POST['ajax']==='registration-form')?array():array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}
	/**
	 * Registration user
	 */
	public function actionRegistration() {

            // Registration form model
            $model = new RegistrationForm;

            // Profile object
            $profile=new Profile;

            // Scenario ?
            $profile->regMode = true;

            // Request object
            $request    = Yii::app()->request;

            // Ajax object
            $ajax       = $request->getPost("ajax");

			// Ajax validator
			if($request->isAjaxRequest && isset($ajax) && $ajax === 'registration-form')
			{
				echo UActiveForm::validate(array($model,$profile));
				Yii::app()->end();
			}

            // Redirect user is it is already logged in
		    if (Yii::app()->user->id) {
		    	$this->redirect(Yii::app()->controller->module->profileUrl);
		    } else {
		    	if(isset($_POST['RegistrationForm'])) {
					$model->attributes      = $_POST['RegistrationForm'];
					$profile->attributes    = ((isset($_POST['Profile'])?$_POST['Profile']:array()));
					if($model->validate()&&$profile->validate())
					{
                        // User salt
                        $salt                   = CoreHelper::generateString(15);

                        // Plain text password
                        $plain_text_password    = $model->password;

                        // Hashed password (with salt)
                        $hashed_password        = UserModule::encrypting($plain_text_password, $salt);

                        // User activation key
                        $activation_key         = UserModule::encrypting(microtime().$hashed_password, $salt);

                        // Account activation key
						$model->activation_key  = $activation_key;

                        // Hashed password
						$model->password        = $hashed_password;

                        // Re-types password
						$model->verifyPassword  = UserModule::encrypting($model->verifyPassword, $salt);

                        // Current registration date and time
                        $model->registered      = time();

                        // Current date and time for login
						$model->last_visit      = ((Yii::app()->controller->module->loginNotActive||(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false))&&Yii::app()->controller->module->autoLogin)?time():0;

                        // User class normal
                        $model->superuser       = 0;

                        // Account status
						$model->status          = ((Yii::app()->controller->module->activeAfterRegister)?User::STATUS_ACTIVE:User::STATUS_NOACTIVE);

                        // Account salt
                        $model->salt            = $salt;

						if ($model->save()) {

                            // Save profile model
							$profile->user_id   = $model->id;

                            // Save profile model
							$profile->save();

                            // Send activation e-mail, if enabled
							if (Yii::app()->controller->module->sendActivationMail) {

                                // URL to be clicked
                                $activation_url = $this->createAbsoluteUrl('/user/activation/activation',array("activation_key" => $model->activation_key, "email" => $model->email));

                                // Send e-mail
                                UserModule::sendMail($model->email,UserModule::t("You registered from {site_name}",array('{site_name}'=>Yii::app()->name)),UserModule::t("Please activate you account go to {activation_url}",array('{activation_url}'=>$activation_url)));
							}

                            // Log-in user after registration
							if ((Yii::app()->controller->module->loginNotActive || (Yii::app()->controller->module->activeAfterRegister && Yii::app()->controller->module->sendActivationMail == false)) && Yii::app()->controller->module->autoLogin) {

                                    // Check identity
                                    $identity=new UserIdentity($model->username,$plain_text_password);

                                    // Authenticate
                                    $identity->authenticate();

                                    // Create session
									Yii::app()->user->login($identity,0);

                                    // Redirect user
									$this->redirect(Yii::app()->controller->module->returnUrl);
							} else {
								if (!Yii::app()->controller->module->activeAfterRegister&&!Yii::app()->controller->module->sendActivationMail) {
									Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Contact Admin to activate your account."));
								} elseif(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false) {
									Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Please {{login}}.",array('{{login}}'=>CHtml::link(UserModule::t('Login'),Yii::app()->controller->module->loginUrl))));
								} elseif(Yii::app()->controller->module->loginNotActive) {
									Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Please check your email or login."));
								} else {
									Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Please check your email."));
								}
								$this->refresh();
							}
						}
					} else $profile->validate();
				}
			    $this->render('/user/registration',array('model'=>$model,'profile'=>$profile));
		    }
	}
}