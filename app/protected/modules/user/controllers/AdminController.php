<?php

class AdminController extends Controller
{
	public $defaultAction = 'admin';
	
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return CMap::mergeArray(parent::filters(),array(
			'accessControl', // perform access control for CRUD operations
		));
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create','update','view'),
				'users'=>UserModule::getAdmins(),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$dataProvider=new CActiveDataProvider('User', array(
			'pagination'=>array(
				'pageSize'=>Yii::app()->controller->module->user_page_size,
			),
		));

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}


	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
		$model = $this->loadModel();
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        // User model
		$model      = new User;

        // Profile model
		$profile    = new Profile;

        // Request object
        $request    = Yii::app()->request;

		if($request->isPostRequest)
		{
            // Salt
            $salt                   = CoreHelper::generateString(15);

            // Map attributes
			$model->attributes      = $request->getPost("User");

            // Generate activation key
			$model->activation_key  = Yii::app()->controller->module->encrypting(microtime().$model->password, $salt);

            // Registration timestamp
            $model->registered      = time();

            // Last visit 0
			$model->last_visit      = 0;

            // Profile object
			$profile->attributes    = $request->getPost("Profile");

            // Initial profile id
			$profile->user_id=0;

            // Validate models
			if($model->validate() && $profile->validate()) {

                // Save hashed and salted user password
				$model->password    = Yii::app()->controller->module->encrypting($model->password, $salt);

                // Try to save user model
                if($model->save()) {

                    // Save profile id from user id
					$profile->user_id= $model->id;

                    // Save profile model
					$profile->save();
				}

                // Redirect
				$this->redirect(array('view','id'=>$model->id));

			} else $profile->validate();
		}

		$this->render('create',array(
			'model'=>$model,
			'profile'=>$profile,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
        // Request object
        $request    = Yii::app()->request;

        // Load user model
		$model      = $this->loadModel();

        // Load users' profile
		$profile    = $model->profile;

		if($request->isPostRequest)
		{
            // Get user model fields from post
			$model->attributes      = $request->getPost("User");

            // Get user profile data
			$profile->attributes    = $request->getPost("Profile");

            // Validate models
			if($model->validate() && $profile->validate()) {

                // Load model of the old password
				$old_password       = User::model()->notsafe()->findByPk($model->id);

                // Check if old password is not the same with the new password
				if (strlen(trim($model->password)) > 0 && $old_password->password !== $model->password) {

                    // Set new password
					$model->password        = Yii::app()->controller->module->encrypting($model->password, $old_password->salt);

                    // Activation key
                    $model->activation_key  = Yii::app()->controller->module->encrypting(microtime().$model->password, $old_password->salt);
				} else {

                    // Just keep the old pw
                    $model->password = $old_password->password;
                }

                // Save user model
				$model->save();

                // Save profile
				$profile->save();

                // Redirect user
				$this->redirect(array('view','id'=>$model->id));
			} else $profile->validate();
		}

        $model->password = "";

		$this->render('update',array(
			'model'=>$model,
			'profile'=>$profile,
		));
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model = $this->loadModel();
			$profile = Profile::model()->findByPk($model->id);
			$profile->delete();
			$model->delete();
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_POST['ajax']))
				$this->redirect(array('/user/admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=User::model()->notsafe()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
	
}