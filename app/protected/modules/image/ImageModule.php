<?php

class ImageModule extends CWebModule
{
    const UPLOAD_DIR = "/user_photos/";
	public function init()
	{
		// import the module-level models and components
		$this->setImport(array(
			'image.models.*',
			'image.components.*',
            'image.helpers.*',
            'application.modules.comments.models.*',
            'application.modules.user.models.*',
            'application.modules.imageShare.models.*',
            'ext.EPhpThumb.EPhpThumb',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

    /**
     * @param $string_identifier
     * @return null|string
     */
    public static function getImageUrlByStringIdentifier($string_identifier)
    {
        $image = Image::getImageByStringIdentifier($string_identifier);

        if($image !== null)
        {
            $path_arr   = explode(DIRECTORY_SEPARATOR, $image->path);
            $path       = (sizeof($path_arr) > 0 ? $path_arr[sizeof($path_arr) - 1] : "");
            $webroot    = Yii::app()->getBaseUrl(true);
            $url        = $webroot."/".$path."/".$image->filename;
            return $url;
        }
        return null;
    }
}
