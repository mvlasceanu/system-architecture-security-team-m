<?php
/**
 * Mihai-Marius Vlasceanu
 * <p>
 * NOTICE OF LICENSE
 * <p>
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author Mihai-Marius Vlasceanu
 * @class UploadController
 * @file UploadController.php
 * @timestamp 16-Mar-15 03:12
 * @copyright Copyright (c) 2015 Mihai-Marius Vlasceanu (mihaivlasceanu@gmail.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class UploadController extends Controller{

    /**
     * Main action
     */
    public function actionIndex()
    {
        // Request object
        $request = Yii::app()->request;

        // Image model
        $model = new Image();

        // Helper
        $_helper = new ImageHelper();

        // Redirect user to log in if guest
        if(Yii::app()->user->isGuest)
        {

            // Page to return after logging in
            $current_page = $this->createAbsoluteUrl('/image/upload');
            Yii::app()->user->returnUrl = $current_page;

            // Do redirect
            return $this->redirect($this->createAbsoluteUrl("/user/login"));
        }

        if($request->isPostRequest)
        {
            // Get post
            $model->attributes  = $request->getPost("Image");

            // Image upload scenario
            $model->setScenario(Image::SCENARIO_IMAGE_UPLOAD);

            // Get the filename from form
            $filename           = CUploadedFile::getInstance($model,'filename');

            // Filename to be saved in db and disk
            $db_file_name       = Yii::app()->user->secret. "_".$model->string_identifier."_".strtolower($filename);

            // Add filename to image
            $model->filename    = $db_file_name;

            // Add logged in user id
            $model->user_id     = Yii::app()->user->id;

            // If image was saved in db
            if($model->save())
            {
                // Save to disk
                $path =  $model->path. '/' . $db_file_name;

                // Get file instance
                $filename->saveFile($path, CUploadedFile::getInstance($model,'filename'));

                $sharedWithIds = CHtml::encode($request->getPost("sharedWithIds"));

                if(strlen(trim($sharedWithIds)) > 0)
                {
                    $ids = explode(",", $sharedWithIds);

                    foreach($ids as $id)
                    {
                        $id_value = (int) $id;

                        if(is_numeric($id_value) && User::idExists($id_value) && !ImageShare::isSharedWith($id_value, $model->id))
                        {
                            $share = new ImageShare();

                            $share->image_id = $model->id;

                            $share->user_id = $id_value;

                            $share->save();

                        }
                    }
                } else {
                    if($model->privacy !== ImageHelper::PRIVACY_PUBLIC)
                    {
                        Yii::app()->user->setFlash("image_private", "No shared users selected. This image is only visible to you.");
                    }
                }

                // Redirect to image's page
                return $this->redirect($this->createAbsoluteUrl('/image/view', array("v" => $model->string_identifier)));
            }
        }


        $this->render("upload", array("model" => $model, "privacy_options" => $_helper::getPrivacyOptions()));
    }
}