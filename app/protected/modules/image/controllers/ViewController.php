<?php
/**
 * Mihai-Marius Vlasceanu
 * <p>
 * NOTICE OF LICENSE
 * <p>
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author Mihai-Marius Vlasceanu
 * @class ViewController
 * @file ViewController.php
 * @timestamp 17-Mar-15 22:58
 * @copyright Copyright (c) 2015 Mihai-Marius Vlasceanu (mihaivlasceanu@gmail.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class ViewController extends Controller {

    /**
     * @param $v
     * @throws CHttpException
     */
    public function actionIndex($v)
    {
        // Request object
        $request = Yii::app()->request;

        // Current user
        $user   = Yii::app()->user;

        // Throw exception if image code is missing from query param
        if(!$v)
        {
            throw new CHttpException(404, "No such image");
        }

        // Image model
        $image = Image::getImageByStringIdentifier($v);

        // Throw exception if image not found/null
        if(!$image)
        {
            throw new CHttpException(404, "No such image 2");
        }

        // Throw exception if the image is not shared publicly
        if(!$image->canBeViewed())
        {
            if($user->isGuest) {
                // URL to return after log in
                $return_url = $this->createAbsoluteUrl("/image/view/", array("v" => $image->string_identifier));
                Yii::app()->user->returnUrl = $return_url;

                // Redirect
                return $this->redirect($this->createAbsoluteUrl("/user/login"));
            }
            throw new CHttpException(403, "This image is not available.!");
        }

        // Init comment model
        $comment = new Comment();

        // Display warning to owner if the image is not shared
        if($image->privacy == ImageHelper::PRIVACY_PRIVATE)
        {
            Yii::app()->user->setFlash("image_private", "This image is only visible to you.");
        }

        // Get list of users who share this picture
        $sharedWith = $image->shares;

        // Shared ids
        $shared_ids = array();

        foreach($sharedWith AS $share)
        {
            array_push($shared_ids, $share->user_id);
        }
        $shared_ids_str = implode(",", $shared_ids);

        // Flag
        $can_view_shares_section = ($image->isOwner($user->id) || Yii::app()->getModule('user')->isAdmin() ? true : false);

        // Check for POST request
        if($request->isPostRequest)
        {

            // Redirect user to log in if guest
            if(Yii::app()->user->isGuest)
            {

                // URL to return after log in
                $return_url = $this->createAbsoluteUrl("/image/view/", array("v" => $image->string_identifier));
                Yii::app()->user->returnUrl = $return_url;

                // Redirect
                return $this->redirect($this->createAbsoluteUrl("/user/login"));
            }

            // If comment is posted
            if($request->getPost("Comment"))
            {

                // Map data from form
                $comment->attributes = $request->getPost("Comment");

                // Add logged in user to comment
                $comment->user_id = Yii::app()->user->id;

                // Add image id to comment
                $comment->image_id = $image->id;

                // Approve comment
                $comment->status = "approved";

                // Validate comment data
                if($comment->validate())
                {

                    // Save comment ot db
                    $comment->save();

                    // Reset last data
                    $comment = new Comment();
                }
            }
        }

        $this->render("view", array("image" => $image,
                                    "comment" => $comment,
                                    "can_view_shares_section" => $can_view_shares_section,
                                    "shared_ids_str" => $shared_ids_str,
                                    "shares" => $sharedWith));
    }
}