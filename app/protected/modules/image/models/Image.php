<?php

/**
 * This is the model class for table "{{images}}".
 *
 * The followings are the available columns in table '{{images}}':
 * @property string $id
 * @property string $string_identifier
 * @property string $name
 * @property string $description
 * @property string $path
 * @property string $filename
 * @property string $uploaded
 * @property string $user_id
 * @property string $privacy
 * @property string $deleted
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class Image extends CActiveRecord
{
    const SCENARIO_IMAGE_UPLOAD = "image_upload";

    const SCENARIO_IMAGE_DELETE = "image_delete";

    private $url;

    private $controller_url;

    private $thumbnail_url;

    public function afterConstruct()
    {
        if($this->path === null) {
            // Path
            $this->path = realpath(Yii::app()->basePath . '/..' . ImageModule::UPLOAD_DIR);
        }

        if($this->string_identifier === null) {
            // Generate a random string
            $this->string_identifier = CoreHelper::generateString(10);
        }

        if(!$this->privacy)
        {
            $this->privacy = ImageHelper::PRIVACY_PUBLIC;
        }
        $this->setUrl();
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{images}}';
	}

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->setUrl();
    }

    public function onAfterFind($event)
    {
        parent::onAfterFind($event);
        $this->getUrl();
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('path, filename, user_id', 'required'),
			array('id', 'length', 'max'=>15, 'on' => array(self::SCENARIO_IMAGE_DELETE)),
			array('string_identifier', 'length', 'max'=>10, 'on' => array(self::SCENARIO_IMAGE_DELETE)),
			array('name', 'length', 'max'=>45),
			array('path', 'length', 'max'=>500),
			array('filename', 'length', 'max'=>80),
            array('filename', 'file', 'types'=> 'jpg, gif, png', 'maxSize'=>1024 * 1024 * 50, 'tooLarge'=>'File has to be smaller than 50MB', 'on' => array(self::SCENARIO_IMAGE_UPLOAD)),
			array('user_id', 'length', 'max'=>11),
			array('privacy', 'length', 'max'=>7),
			array('deleted', 'length', 'max'=>1),
			array('description, uploaded', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, string_identifier, name, description, path, filename, uploaded, user_id, privacy, deleted', 'safe', 'on'=>'search'),
            array('url, sharedWithIds', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user'      => array(self::BELONGS_TO,  'User',         'user_id'),
            'comments'  => array(self::HAS_MANY,    'Comment',      'image_id'),
            'shares'    => array(self::HAS_MANY,    'ImageShare',   'image_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'                => 'ID',
			'string_identifier' => 'String Identifier',
			'name'              => 'Name',
			'description'       => 'Description',
			'path'              => 'Path',
			'filename'          => 'Filename',
			'uploaded'          => 'Uploaded',
			'user_id'           => 'User',
			'privacy'           => 'Privacy',
			'deleted'           => 'Deleted',
            'url'               => 'URL',
		);
	}

    /**
     * @param string $name
     * @return bool
     */
    public function hasAttribute($name)
    {
        if($name == 'url')
            return true;
        return parent::hasAttribute($name);
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('string_identifier',$this->string_identifier,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('filename',$this->filename,true);
		$criteria->compare('uploaded',$this->uploaded,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('privacy',$this->privacy,true);
		$criteria->compare('deleted',$this->deleted,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Image the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return bool
     */
    public function beforeSave() {

        // Current date and time
        $date_time              = new DateTime('NOW');
        $current_dt             = $date_time->format("Y-m-d H:i:s");
        $this->uploaded         = $current_dt;

        return parent::beforeSave();
    }


    /**
     * @param int $length
     * @return string
     */
    public static function generateUniqueStringIdentifier($length = 12)
    {
        $string     = CoreHelper::generateString($length);

        $image      = Image::model()->findByAttributes(array('string_identifier' => $string));
        if($image == null)
        {
            return $string;
        } else {
            return Image::generateUniqueStringIdentifier($length);
        }
    }

    /**
     * @param $string_identifier
     * @return array|mixed|null|static
     */
    public static function getImageByStringIdentifier($string_identifier)
    {
        return Image::model()->findByAttributes(array('string_identifier' => $string_identifier));
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        if($this->url)
            return $this->url;

        $this->setUrl();

        return $this->url;
    }

    public function setUrl()
    {
        if($this->url == null && $this->filename)
        {
            $path_arr   = explode(DIRECTORY_SEPARATOR, $this->path);
            $path       = (sizeof($path_arr) > 0 ? $path_arr[sizeof($path_arr) - 1] : "");
            $webroot    = Yii::app()->getBaseUrl(true);
            $url        = $webroot."/".$path."/".$this->filename;
            $this->url  = $url;
        }
    }


    /**
     * Thumbnail url
     *
     * @param int $width
     * @param int $height
     * @return string
     */
    public function getThumbnailUrl($width = 100, $height = 100)
    {
        try {
            if($this->thumbnail_url == null && $this->filename)
            {
                $path_arr               = explode(DIRECTORY_SEPARATOR, $this->path);
                $path                   = (sizeof($path_arr) > 0 ? $path_arr[sizeof($path_arr) - 1] : "");
                $webroot                = Yii::app()->getBaseUrl(true);
                $scale_str              = $width ."x". $height . "_";
                $url                    = $webroot."/".$path."/th/".$scale_str.$this->filename;
                $this->thumbnail_url    = $url;
                $original_file          = $path."/".$this->filename;
                $thumbnail              = $path."/th/".$scale_str.$this->filename;
                if(!is_file($thumbnail) && is_file($original_file)) {
                    $thumb = new EPhpThumb();
                    $thumb->init();
                    $thumb=Yii::app()->phpThumb->create(Yii::getPathOfAlias('webroot') . "/user_photos/" . $this->filename);
                    $thumb->resize($width,$height);
                    $thumb->save('./user_photos/th/' .$scale_str. $this->filename);
                }
                return $this->thumbnail_url;
            }
        } catch(CException $ce)
        {
            return $this->path."/".$this->filename;
        }
    }

    public function setControllerUrl()
    {
        if($this->string_identifier && $this->filename && $this->user_id)
        {
            $this->controller_url = Yii::app()->createAbsoluteUrl("image/view", array("v" => $this->string_identifier));
        }
    }

    /**
     * @return mixed
     */
    public function getControllerUrl()
    {
        if($this->controller_url)
            return $this->controller_url;

        $this->setControllerUrl();
        return $this->controller_url;
    }

    /**
     * @param $userId
     * @return bool
     */
    public function isOwner($userId)
    {
        return ($this->user_id === $userId);
    }

    /**
     * @return bool
     */
    public function canBeViewed()
    {
        $user = Yii::app()->user;
        if(Yii::app()->getModule('user')->isAdmin()
            || isset($user) && isset($user->id) && $this->user_id === $user->id
            || $this->privacy == ImageHelper::PRIVACY_PUBLIC
            || $this->privacy == ImageHelper::PRIVACY_CUSTOM && ImageShare::isSharedWith($user->id, $this->id))
            return true;
        return false;
    }

    /**
     * @return array|mixed|null|static
     */
    public static function getPublicPictures()
    {
        return Image::model()->findAllByAttributes(array('privacy' => ImageHelper::PRIVACY_PUBLIC));
    }
}
