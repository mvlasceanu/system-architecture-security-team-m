<?php
/**
 * Mihai-Marius Vlasceanu
 * <p>
 * NOTICE OF LICENSE
 * <p>
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author Mihai-Marius Vlasceanu
 * @class ${NAME}
 * @file view.php
 * @timestamp 17-Mar-15 23:12
 * @copyright Copyright (c) 2015 Mihai-Marius Vlasceanu (mihaivlasceanu@gmail.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/* @var $this ViewController */

$this->breadcrumbs=array(
    ucfirst($this->module->id),
    $image->name,
);
?>
<?php /*********** Page Title ************/ ?>
<h1><?php echo $image->name; ?></h1>


<?php /*********** Flash Messages ************/ ?>
<?php if(Yii::app()->user->hasFlash('image_private')): ?>
<div class="ui-flash ui-flash-warning" id="callout-inline-form-labels">
    <h4 id="warning"><?php echo Yii::t("app", "Warning"); ?></h4>
    <p><?php echo Yii::app()->user->getFlash('image_private'); ?></p>
</div>
<?php endif; ?>

<?php /*********** Image Container ************/ ?>
<div class="image-container">
    <img src="<?php echo $image->getUrl(); ?>" width="100%" />
</div>

<?php /*********** Image Info ************/ ?>
<div class="image-info">
    <?php $ownerLink = CHtml::link($image->user->username, $this->createAbsoluteUrl("/user/user/view/", array("id" => $image->user->id))); ?>
    <div id="image-info-row">
        <span class="user-avatar"><?php echo CHtml::image(Yii::app()->getBaseUrl(true). "/user_photos/avatars/default_avatar.png"); ?></span>
        <span class="image-info-title"><?php echo $ownerLink; ?> <spam id="image-info-time"><?php echo CoreHelper::getTimeSince($image->uploaded, 1); ?></spam></span>
        <p><?php echo CHtml::encode($image->description); ?></p>
    </div>
</div>

<?php if($can_view_shares_section): ?>
<div class="share-info" ng-controller="UpdatePrivacyController">
    <strong><?php echo Yii::t("app", "Shared with"); ?>:</strong>
    <?php echo CHtml::dropDownList('privacy', $image->privacy, ImageHelper::getPrivacyOptions(), array("ng-model" => "privacy", "ng-selected" => $image->privacy, "ng-change" => "changePrivacy()")); ?>
    <div class="row" id="ajaxUsers" ng-show="privacy=='<?php echo ImageHelper::PRIVACY_CUSTOM; ?>'">
        <div isteven-multi-select
             input-model="shareUsersIn"
             output-model="shareUsersOut"
             button-label="username"
             item-label="username"
             tick-property="ticked"
             class="ng-isolate-scope"
             on-item-click="watchSelected(data)"
             on-select-all="watchSelectedAll(true)"
             on-select-none="watchSelectedAll(false)"
             on-close="refreshPage()"
             id="share_with"
            >
        </div>
    <br />
    <?php foreach($shares AS $share): ?>
        <?php echo CHtml::link($share->user->username, $this->createAbsoluteUrl("/user/user/view/", array("id" => $share->user_id))); ?>,
    <?php endforeach; ?>
    </div>
    <?php echo CHtml::hiddenField('sharedWithIds', $shared_ids_str, array("ng-model" => "sharedWithIds", "ng-value" => "sharedWithIds")); ?>
</div>
<?php endif; ?>

<?php /*********** Comment Form ************/ ?>
<div class="form">
    <?php $form=$this->beginWidget('UActiveForm', array(
        'id'=>'comment-form')); ?>
    <?php echo $form->errorSummary(array($comment)); ?>
    <h2><?php echo Yii::t("app", "Comments"); ?> (<?php echo sizeOf($image->comments); ?>)</h2>
    <?php echo $form->textArea($comment, "text", array("class" => "comment-box")); ?>

    <?php echo CHtml::submitButton(Yii::t("app", "Add comment")); ?>
    <?php $this->endWidget(); ?>

    <?php /*********** Comments list ************/ ?>
    <div class="comments">
        <?php foreach($image->comments AS $com): ?>
        <?php $user_link = CHtml::link($com->user->username, $this->createAbsoluteUrl("/user/user/view/", array("id" => $com->user->id))); ?>
            <div id="comment-row">
                <span class="user-avatar"><?php echo CHtml::image(Yii::app()->getBaseUrl(true). "/user_photos/avatars/default_avatar.png"); ?></span>
                <span class="comment-title"><?php echo $user_link; ?> <spam id="comment-time"><?php echo CoreHelper::getTimeSince($com->added, 1); ?>:</spam></span>
                <p><?php echo CHtml::encode($com->text); ?></p>
            </div>
        <?php endforeach; ?>
    </div>
</div>


