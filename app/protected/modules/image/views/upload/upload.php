<?php
/* @var $this ImageController */
/* @var $model Image */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'image-upload-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description'); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'filename'); ?>
        <?php echo $form->fileField($model,'filename'); ?>
        <?php echo $form->error($model,'filename'); ?>
    </div>

    <div ng-controller="GetUsersController">
        <div class="row">
            <?php echo $form->labelEx($model,'privacy'); ?>
            <?php echo $form->dropDownList($model,'privacy', $privacy_options, array("ng-model" => "privacy", "ng-selected" => ImageHelper::PRIVACY_PRIVATE)); ?>
            <?php echo $form->error($model,'privacy'); ?>
        </div>

        <div class="row" id="ajaxUsers" ng-show="privacy=='<?php echo ImageHelper::PRIVACY_CUSTOM; ?>'">
            <?php echo CHtml::label('Share with', "#share_with"); ?>
            <div isteven-multi-select
                 input-model="shareUsersIn"
                 output-model="shareUsersOut"
                 button-label="username"
                 item-label="username"
                 tick-property="ticked"
                 class="ng-isolate-scope"
                 on-item-click="watchSelected(data)"
                 on-select-all="watchSelectedAll(true)"
                 on-select-none="watchSelectedAll(false)"
                 id="share_with"
                >
            </div>
        </div>

        <?php echo CHtml::hiddenField('sharedWithIds', "0", array("ng-value" => "selectedSharesHidden")); ?>
    </div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->