<?php
/**
 * Mihai-Marius Vlasceanu
 * <p>
 * NOTICE OF LICENSE
 * <p>
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @author Mihai-Marius Vlasceanu
 * @class ImageHelper
 * @file ImageHelper.php
 * @timestamp 16-Mar-15 03:21
 * @copyright Copyright (c) 2015 Mihai-Marius Vlasceanu (mihaivlasceanu@gmail.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class ImageHelper {
    const PRIVACY_PUBLIC     = "public";
    const PRIVACY_CUSTOM     = "custom";
    const PRIVACY_PRIVATE    = "private";
    const IMAGE_STR_ID_REGEX = "([a-zA-Z09-_]+)";
    /**
     * List of possible privacy options
     *
     * @return array
     */
    public static function getPrivacyOptions()
    {
        return array(
            self::PRIVACY_PUBLIC => ucfirst(self::PRIVACY_PUBLIC),
            self::PRIVACY_CUSTOM => ucfirst(self::PRIVACY_CUSTOM),
            self::PRIVACY_PRIVATE => ucfirst(self::PRIVACY_PRIVATE)
        );
    }
}