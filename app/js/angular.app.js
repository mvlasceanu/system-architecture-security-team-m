var psa = angular.module("psa", ["isteven-multi-select"]);

psa.controller( 'GetUsersController' , function ($scope, $http) {
    // Default selected value for uploaded image privacy
    $scope.privacy = "private";

    // IDs of all users
    $scope.shareUsersIn = [];

    // IDs of modified users
    $scope.shareUsersOut= [];

    // Populate users from AJAX
    $scope.shareUsersIn = $http.get("/user/api/GetUsersWithoutSelf")
    .success(function(data, status, headers, config) {
        $scope.shareUsersIn = data.users;
    }).error(function(data, status, headers, config) {
        alert("AJAX failed!");
    });

    // Copy
    $scope.shareUsersOut = $scope.shareUsersIn;

    // Store the ids of the selected users to share the image with
    $scope.selectedIds = [];

    // Text value of hidden input holder
    $scope.selectedSharesHidden = "";

    // Handles input hidden based on what is selected from users list
    $scope.watchSelected = function(data)
    {
        if(data.ticked) {
            $scope.selectedIds.push(data.id);
        } else {
            $scope.remove($scope.indexOf(data.id));
        }

        $scope.refreshHidden();
    };

    // Handles the hidden input value when the select all/select none button is pressed
    $scope.watchSelectedAll = function(ticked)
    {
        angular.forEach( $scope.shareUsersIn, function(e) {
            e.ticked = ticked;
            $scope.watchSelected(e);
        });
    };

    // refreshes the data in the input field
    $scope.refreshHidden = function()
    {
        $scope.selectedSharesHidden = $scope.selectedIds.join(",");
    };

    // Get the index of an item in the array
    $scope.indexOf = function(item)
    {
        for(var i = 0; i < $scope.selectedIds.length; i++ )
        {
            if($scope.selectedIds[i] == item) {
                return i;
            }
        }
    };

    // Remove item from array
    $scope.remove = function(index){
        $scope.selectedIds.splice(index, 1);
    };
});


psa.controller("UpdatePrivacyController", [ '$scope', '$http', '$location', function($scope, $http, $location) {

    // Initialize privacy dropdown
    var e = document.getElementById("privacy");
    var strUser = e.options[e.selectedIndex].value;

    // Selected privacy value
    $scope.privacy = strUser;

    // Current value of hidden field
    $scope.sharedWithIds = document.getElementsByName("sharedWithIds")[0].value;

    // Store it as array
    $scope.selectedIds = $scope.sharedWithIds.split(",");

    // Object array for all users
    $scope.allUsersIn = [];

    // Objects array of selected updated users
    $scope.allUsersOut= [];

    // Get URL query params
    $scope.urlVars = function()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    };

    // Ajax call for all users
    $scope.allUsersIn = $http.get("/user/api/getUsersWithoutSelfWithShare", {params: {image: $scope.urlVars()['v']}}, {headers: {'Content-Type': 'application/json'}})
        .success(function(data, status, headers, config) {
            $scope.shareUsersIn = data.users;
        }).error(function(data, status, headers, config) {
            alert("AJAX failed!");
        });
    $scope.shareUsersOut = $scope.shareUsersIn;

    // Ads/removes shareing users and makes ajax calls to update db
    $scope.watchSelected = function(data)
    {
        if(data.ticked)
        {
            $scope.selectedIds.push(data.id);
            $http.get("/user/api/addShare", {params: {image: $scope.urlVars()['v'], user_id: data.id}}, {headers: {'Content-Type': 'application/json'}})
                .success(function(data, status, headers, config) {
                    $scope.shareUsersIn = data.users;
                    console.log(data);
                }).error(function(data, status, headers, config) {
                    alert("AJAX failed!");
                });
        } else {
            $scope.remove($scope.indexOf(data.id));
            $http.get("/user/api/removeShare", {params: {image: $scope.urlVars()['v'], user_id: data.id}}, {headers: {'Content-Type': 'application/json'}})
                .success(function(data, status, headers, config) {
                    console.log(data);
                    $scope.shareUsersIn = data.users;
                }).error(function(data, status, headers, config) {
                    alert("AJAX failed!");
                });
        }

        $scope.refreshHidden();
    };

    // Same as above
    $scope.watchSelectedAll = function(ticked)
    {
        angular.forEach( $scope.shareUsersIn, function(e) {
            e.ticked = ticked;
            $scope.watchSelected(e);
        });
    };

    // Same as above
    $scope.refreshHidden = function()
    {
        $scope.sharedWithIds = $scope.selectedIds.join(",");
    };

    // Same as above
    $scope.indexOf = function(item)
    {
        for(var i = 0; i < $scope.selectedIds.length; i++ )
        {
            if($scope.selectedIds[i] == item) {
                return i;
            }
        }
    };

    // Same as above
    $scope.remove = function(index){
        $scope.selectedIds.splice(index, 1);
    };

    // Page reload
    $scope.refreshPage = function() {
        window.location.reload();
    };

    // AJAX call to update image privacy
    $scope.changePrivacy = function() {
        var e = document.getElementById("privacy");
        var new_privacy = e.options[e.selectedIndex].value;
        var image = $scope.urlVars()['v'];

        if(new_privacy.toLowerCase().trim() === "public" || new_privacy.toLowerCase().trim() === "private")
        {
            $http.get("/user/api/changePrivacy", {params: {image: image, privacy: new_privacy}}, {headers: {'Content-Type': 'application/json'}})
                .success(function(data, status, headers, config) {
                    $scope.refreshPage();
                }).error(function(data, status, headers, config) {
                    alert("AJAX failed!");
                });
        }

    };
}]);
